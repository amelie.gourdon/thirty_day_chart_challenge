spiralStack <-
    function(values,
             col = 1:length(values),
             n = 12,
             lwd = 2,
             ...) {
        # Archimedean spiral, cartesian coordinates.
        a <- 1     # Starting point of the spiral
        b <- 1   # Space between each ring.
        n <- n    # Number of rings.
        theta <- seq(0, n * (2 * pi), length.out = 1300)
        
        # Segment lengths
        seglen <- (a / 2) * (theta * sqrt(1 + theta ^ 2) +
                                 log(theta + sqrt(1 + theta ^ 2)))
        
        # Calculate percentages and part lengths.
        p <- values / sum(values)
        plen <- p * seglen[length(seglen)]
        
        # Cartesian coordinates.
        x <- (a + b * theta) * cos(theta)
        y <- (a + b * theta) * sin(theta)
        
        # Start plot.
        par(lend = 1)
        plot(
            x,
            y,
            type = "n",
            asp = 1,
            bty = "n",
            axes = FALSE,
            xlab = "",
            ylab = "",
            ...
        )
        
        
        # Draw arc segment for each value.
        prev_index <- 1
        for (i in 1:length(values)) {
            if (i == length(values)) {
                # Handle rounding issues.
                curr_index <- length(seglen)
            } else {
                curr_index <- which(seglen >= cumsum(plen)[i])[1]
            }
            
            curr_x <- x[prev_index:curr_index]
            curr_y <- y[prev_index:curr_index]
            curr_theta <- theta[prev_index:curr_index]
            lines(curr_x, curr_y, lwd = lwd, col = col[i])
            
            prev_index <- curr_index
        }
    }